# Description
This is a little remote git flask project, to be able to ask via a curl command or via a browser for git output.

# Development

```shell
python3 -m venv ./venv-foo
. ./venv-foo/bin/activate
virtualenv venv --python=python3.10.8
export FLASK_DEBUG=1
export FLASK_APP=remotegit
flask run -p 8000
```

# Usage

Different command that can be answered without having a git tree locally.

## git describe

For the 'git describe' to show the describe on that ref or sha in the specified repository.

```shell
curl -sSL "http://127.0.0.1:8000/git-describe?tree=https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
```
This can be the following two git commands:
```shell
git tag
```

## git tags

For the 'git branch --all' to list all the tags in the specified repository.

```shell
curl -sSL "http://127.0.0.1:8000/git-tags?tree=https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
```
This can be the following two git commands:
```shell
git tag
```

## git branches

For the 'git branch --all' to list all the branches in the specified repository.

```shell
curl -sSL "http://127.0.0.1:8000/git-branches?tree=https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
```
This can be the following two git commands:
```shell
git branch --all
```

## git rev-list

For the 'git rev-list' we need old-sha, new-sha and bisect that can
be either 'all' or 'vars' in the specified repository.

```shell
curl -sSL "http://127.0.0.1:8000/git-rev-list?tree=https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git&old=v6.4&new=v6.5&bisect=all"
```
This can be the following two git commands:
```shell
git rev-list --bisect-vars old-sha..new-sha
```

```shell
git rev-list --bisect-all old-sha..new-sha
```

## git bisect

There's possible to do N steps bisect and get back a file with replay leafs and
the sha's that should be tested.

```shell
git bisect old-sha new-sha steps
```

When the first step of bisect are done, then you give back to the tool a replay
file to perform the rest of the bisect steps.


