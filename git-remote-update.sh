#!/bin/bash
# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2023-present Linaro Limited
#
# SPDX-License-Identifier: MIT

repository_dir=${1}

if [[ -z ${repository_dir} ]]; then
	echo "please pass in base repository dir"
	exit 1
fi

for repo in $(find ${repository_dir} -type d -name '.git')
do
	repo_path=$(dirname $repo)
	echo $repo_path
	git -C $repo_path remote update
	git -C $repo_path fetch --tags --all
done
