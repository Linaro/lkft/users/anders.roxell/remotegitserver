# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2023-present Linaro Limited
#
# SPDX-License-Identifier: MIT
#
# curl -sSL "http://127.0.0.1:8000/git-describe?tree=https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git&ref=linux-6.6.y"

from datetime import datetime
from flask import Flask, request
from pathlib import Path
from urllib.parse import urlparse

import logging
import subprocess as sp
import os
import tempfile
import shutil

app = Flask("remotegit")
app.logger.setLevel(logging.INFO)  # Set log level to INFO
handler = logging.FileHandler('app.log')  # Log to a file
app.logger.addHandler(handler)

app.config['MAX_CONTENT_LENGTH'] = 500 * 1024 # 500Kb

def get_tree_base_path(tree):
    url = urlparse(tree)

    tree_base_path = os.path.dirname(f"{url.netloc}/{url.path}")
    tree_name = os.path.basename(url.path).replace(".git", "")
    tree_base_path = Path("git_repos") / tree_base_path
    tree_base_path.mkdir(parents=True, exist_ok=True)
    tree_base_path /= tree_name

    return tree_base_path

def setup_tree_and_fetch(tree, tree_base_path):
    if not (tree_base_path).exists():
        git_clone(tree, tree_base_path)
    else:
        git_remote_update(tree_base_path)

    git_fetch_all(tree, tree_base_path)

def run_git(*args):
    cmd = ["git"] + list(args)
    app.logger.info(f"Running {cmd}")
    proc = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE)
    stdout, stderr = proc.communicate()
    proc.ok = proc.returncode == 0

    proc.out = stdout.decode()
    proc.err = stderr.decode()
    return proc

def git_fetch_all(tree, tree_base_path):
    return run_git("-C", tree_base_path, "fetch", "--tags", "--all")

def git_clone(tree, tree_base_path):
    return run_git("clone", tree, tree_base_path)

def git_remote_update(tree_base_path):
    ret = run_git("-C", tree_base_path, "remote", "update")
    app.logger.info(f"{ret.out}, {ret.err}")
    return ret

def git_checkout(tree_base_path, ref):
    if ref is None:
        ret = run_git("-C", tree_base_path, "checkout", ref)
    else:
        ret = run_git("-C", tree_base_path, "checkout")
    app.logger.info(f"{ret.out}, {ret.err}")
    return ret

def get_sha_from_ref(tree_base_path, ref):
    if ref:
        ret = run_git("-C", tree_base_path, "show-ref", ref)
    app.logger.info(f"{ret.out}, {ret.err}")
    return ret

def git_describe(tree_base_path, sha):
    if sha is None:
        ret = run_git("-C", tree_base_path, "describe")
    else:
        ret = run_git("-C", tree_base_path, "describe", sha)
    app.logger.info(f"{ret.out}, {ret.err}")
    return ret

def git_list_tags(tree_base_path):
    ret = run_git("-C", tree_base_path, "tag")
    app.logger.info(f"{ret.out}, {ret.err}")
    return ret

def git_list_branches(tree_base_path):
    ret = run_git("-C", tree_base_path, "branch", "--all")
    app.logger.info(f"{ret.out}, {ret.err}")
    return ret

def git_rev_list(tree_base_path, bisect, old, new, exclueds):
    if exclueds is None:
        ret = run_git("-C", tree_base_path, "rev-list", "--bisect-"+bisect, old+".."+new)
        app.logger.info(f"ssss")
    else:
        app.logger.info(f"llll")
        exclueds = exclueds.replace(",", " ")
        ret = run_git("-C", tree_base_path, "rev-list", "--bisect-"+bisect, "--not", exclueds, old+".."+new)
    return ret

class Data:
    def __init__(self, name, sha, log, filename):
        self.name = name
        self.sha = sha
        self.log = log
        self.filename = filename

def git_find_good_bad(bisect_base_path, mylist, steps):
    # nodes = 2^steps-1
    if int(steps) > 1:
        for step in range(int(steps)-1):
            app.logger.info(f"skit: {step=} {steps=}")
            for node in range(2**step):
                index = (2**step)+node-1

                out = git_replay_good_bad(mylist, bisect_base_path, index, "good")
                out = git_replay_good_bad(mylist, bisect_base_path, index, "bad")
    out = ""
    for i in range(len(mylist)-2**(int(steps)-1), len(mylist)):
        out += f"\n==SPLITME==\nsha={mylist[i].sha}\n{mylist[i].log}"

    out += f"\n==SPLITME==\nSHAs\n"
    for i in range(len(mylist)):
        out += f" {mylist[i].sha}"

    return out


def git_bisect_replay(bisect_base_path, good_bad, replay_file, steps):

    mylist = []

    filename = bisect_base_path + '/replay-input.log'
    replay_file.save(filename)
    ret = run_git("-C", bisect_base_path, "bisect", "replay", filename)
    ret = run_git("-C", bisect_base_path, "bisect", good_bad)
    sha = run_git("-C", bisect_base_path, "rev-list", "HEAD", "-1")
    ret = run_git("-C", bisect_base_path, "bisect", "log")
    filename = bisect_base_path + '/bisect-root.log'
    with open(filename, mode="x") as file:
        file.write(ret.out)
    data = Data("root", sha.out, ret.out, filename)
    mylist.append(data)

    return git_find_good_bad(bisect_base_path, mylist, steps)

def git_bisect(bisect_base_path, old, new, steps):

    mylist = []

    ret = run_git("-C", bisect_base_path, "bisect", "start", new, old)
    sha = run_git("-C", bisect_base_path, "rev-list", "HEAD", "-1")
    ret = run_git("-C", bisect_base_path, "bisect", "log")
    filename = bisect_base_path + '/bisect-root.log'
    with open(filename, mode="x") as file:
        file.write(ret.out)
    data = Data("root", sha.out, ret.out, filename)
    mylist.append(data)

    return git_find_good_bad(bisect_base_path, mylist, steps)

def git_replay_good_bad(mylist, bisect_base_path, index, good_bad):
    #if step = 1
    ret = run_git("-C", bisect_base_path, "bisect", "reset")
    app.logger.info(f"{mylist[index].log=}")
    ret = run_git("-C", bisect_base_path, "bisect", "replay", mylist[index].filename)
    ret = run_git("-C", bisect_base_path, "bisect", good_bad)
    sha = run_git("-C", bisect_base_path, "rev-list", "HEAD", "-1")
    ret = run_git("-C", bisect_base_path, "bisect", "log")
    filename = bisect_base_path + f"/bisect-{good_bad}-{index}.log"
    with open(filename, mode="x") as file:
        file.write(ret.out)
    data = Data(good_bad, sha.out, ret.out, filename)
    #data = Data(good_bad, "sha", mylist[index].log)
    mylist.append(data)

@app.route("/git-describe")
def get_git_describe():
    # TODO check if tree and branch exist
    tree = request.args.get("tree")
    ref = request.args.get("ref")
    sha = request.args.get("sha")
    if tree is None:
        return "Error: tree {tree} not found", 400
    if ref and sha:
        return f"Error: Can only handle one at the time, not both ref: {ref} and sha: {sha}", 400

    tree_base_path = get_tree_base_path(tree)

    setup_tree_and_fetch(tree, tree_base_path)

    if ref:
        ret = get_sha_from_ref(tree_base_path, ref)
        sha = ret.out.split(" ")[0]

    describe = git_describe(tree_base_path, sha)

    return f"{describe.out}"

@app.route("/git-list-tags")
def get_git_list_tags():
    # TODO check if tree and branch exist
    tree = request.args.get("tree")
    if tree is None:
        return "tree not found", 400

    tree_base_path = get_tree_base_path(tree)

    setup_tree_and_fetch(tree, tree_base_path)

    tags = git_list_tags(tree_base_path)

    return f"{tags.out}"

@app.route("/git-list-branches")
def get_git_list_branches():
    # TODO check if tree and branch exist
    tree = request.args.get("tree")
    if tree is None:
        return "tree not found", 400

    tree_base_path = get_tree_base_path(tree)

    setup_tree_and_fetch(tree, tree_base_path)

    branches = git_list_branches(tree_base_path)

    return f"{branches.out}"

@app.route("/git-rev-list")
def get_git_rev_list():
    # TODO check if tree and branch exist
    tree = request.args.get("tree")
    bisect = request.args.get("bisect")
    old = request.args.get("old")
    new = request.args.get("new")
    exclueds = request.args.get("exclued")
    if tree is None:
        return "tree not found", 400

    tree_base_path = get_tree_base_path(tree)

    setup_tree_and_fetch(tree, tree_base_path)

    revlist = git_rev_list(tree_base_path, bisect, old, new, exclueds)

    return f"{revlist.out}"

# curl -sSL -F 'file=@bisect.replay_file.log' "http://127.0.0.1:8000/git-bisect-replay?tree=https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git&good_bad="good"&steps=3&new=v6.5"
@app.route("/git-bisect-replay", methods=['POST'])
def get_git_bisect_replay():
    tree = request.args.get("tree")
    good_bad = request.args.get("good_bad")
    new = request.args.get("new")
    steps = request.args.get("steps") or 1

    # check if the post request has the file part
    if 'file' not in request.files:
        # FIXME
        return "no file found", 400
    replay_file = request.files['file']
    # If the user does not select a file, the browser submits an
    # empty file without a filename.
    if replay_file.filename == '':
        #FIXME
        return "no filename found", 400

    if tree is None:
        return "tree not found", 400

    tree_base_path = get_tree_base_path(tree)

    setup_tree_and_fetch(tree, tree_base_path)

    bisect_dir, tmp_branch = create_git_worktree(tree_base_path, new)

    bisect = git_bisect_replay(bisect_dir, good_bad, replay_file, steps)

    cleanup_git_worktree(tree_base_path, bisect_dir, tmp_branch)

    return f"{bisect}"
def create_git_worktree(tree_base_path, new):
    now = datetime.now()
    bisectlog_prefix = now.strftime("%Y%m%d_%H%M%S")
    tmp_branch = f"bisect-{bisectlog_prefix}"
    bisect_dir = f"/tmp/bisecting-{bisectlog_prefix}"

    ret = run_git("-C", tree_base_path, "worktree", "add", "-b", tmp_branch, bisect_dir, new)

    return bisect_dir, tmp_branch

def cleanup_git_worktree(tree_base_path, bisect_dir, tmp_branch):
    shutil.rmtree(bisect_dir)
    ret = run_git("-C", tree_base_path, "worktree", "prune")
    ret = run_git("-C", tree_base_path, "branch", "-D", tmp_branch)

@app.route("/git-bisect")
def get_git_bisect():
    tree = request.args.get("tree")
    old = request.args.get("old")
    new = request.args.get("new")
    steps = request.args.get("steps") or 1

    if tree is None:
        return "tree not found", 400

    tree_base_path = get_tree_base_path(tree)

    setup_tree_and_fetch(tree, tree_base_path)

    bisect_dir, tmp_branch = create_git_worktree(tree_base_path, new)

    bisect = git_bisect(bisect_dir, old, new, steps)

    cleanup_git_worktree(tree_base_path, bisect_dir, tmp_branch)

    return f"{bisect}"
